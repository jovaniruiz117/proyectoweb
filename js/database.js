import { ref, push, getDatabase } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { app } from './app.js';

const dataForm = document.getElementById('dataForm');
const successMessage = document.getElementById('successMessage');

dataForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const cantidad = dataForm.cantidad.value;
    const descripcion = dataForm.descripcion.value;
    const nombre = dataForm.nombre.value;
    const status = dataForm.status.value;
    const tipodefrag = dataForm.tipodefrag.value;
    const url = dataForm.url.value;
    const precio = dataForm.precio.value;

    const db = getDatabase(app);
    const dataRef = ref(db, 'Productos'); // Access the 'Productos' node in the database

    // Create a new object with the data entered
    const newData = {
        cantidad: cantidad,
        descripcion: descripcion,
        nombre: nombre,
        status: status,
        tipodefrag: tipodefrag,
        url: url,
        precio: precio
    };

    // Add the new data to the 'Productos' node using push
    const newEntryRef = push(dataRef, newData);

    newEntryRef.then(() => {
        successMessage.innerText = 'Data added successfully to the database with ID: ' + newEntryRef.key;
        successMessage.style.display = 'block';
        // Clear the form
        dataForm.reset();
    }).catch((error) => {
        console.error('Error adding data to the database: ', error);
    });
});

