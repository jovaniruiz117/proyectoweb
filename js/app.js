import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

const firebaseConfig = {
  apiKey: "AIzaSyB6fMwbSSh2qaCl_5Z490nneOryODGkNP4",
  authDomain: "administrador-web-8984a.firebaseapp.com",
  databaseURL: "https://administrador-web-8984a-default-rtdb.firebaseio.com",
  projectId: "administrador-web-8984a",
  storageBucket: "administrador-web-8984a.appspot.com",
  messagingSenderId: "98975475879",
  appId: "1:98975475879:web:4eb755463d1dece72bd382"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export { app }
export const auth = getAuth(app);
export const storage = getStorage(app);
export const database = getDatabase(app);
